help:QV: ## show this command
	awk 'BEGIN {FS = "\t|:.*?## "};
	/[ \t]##[ \t]/ {printf "\033[36m%-20s\033[0m %s\n", $1, $NF}' \
		mkfile \
	| sort

start:V: ## setup a development environment
	virtualenv .venv
        . .venv/bin/activate
	python -m pip install -r requirements.txt
